# Multi-objective optimisation with Gaussian processes 

## Description
"n" dimensional input with "m" objectives example of a Pareto front with the BOSS code.
In these examples we reproduce the pareto front as produced by mathlab in the [link](https://www.mathworks.com/help/gads/pareto-front-for-two-objectives.html
) below
"https://www.mathworks.com/help/gads/pareto-front-for-two-objectives.html"
For 3D inputs and 3 objectives,  we try to reproduce these [results](https://www.mathworks.com/help/gads/plot-3-d-pareto-set.html)


The difference in these methods is that here we calculate the pareto front using GP functions with an RBF kernel. 

## Use
```python
python3 example_2D.py
python3 example_2D_ICM.py
python3 example_3D.py
python3 example_3D_ICM.py
```
## Result
BOSS should produce these image if everything ran correctly
![2D results](./example_2D_pareto_front.png)

The results seem to reproduce the analytical results accurately.
![matlab_result](./Screenshot_MOO_matlab.png)


3D example: Below is the pareto front for a 3D input function and 3 objectives
![3D_result](./example_3D.gif)
The GP models for the 3 models are
![3D_models](./example_3D_models.png)


## Command line interface (CLI)
In the Command Line folder, we can use a single line in the terminal to get the pareto front/optimal solution from GP functions using past BOSS runs.

```shell
boss f gp_f0.rst gp_f1.rst
```

To change the mesh size for the calculations, add a keyword "pf_mesh" to the first .rst file. By default, the mesh size is 30. 
For our case, if you want to have a smaller mesh for a quicker calculation. Add "pf_mesh 10" to the gp_f0.rst file. This would be faster since the mesh size is smaller.


## Learning objectives
1. Using GPs to optimise any nD function for any number of objectives
2. Using various single GPs to optimise functions or using the ICM model to do the same

## Feedback
Send feedback to ransell.dsouza@utu.fi/ ransell.d@gmail.com
