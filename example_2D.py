###################################################################################
# 2D example of a Pareto front
# An example to reproduce the pareto front as produced by mathlab in the link below
# https://www.mathworks.com/help/gads/pareto-front-for-two-objectives.html
# Here we calculate the pareto front using GP functions with an RBF kernel. 
###################################################################################

# Import
from boss.bo.bo_main import BOMain

#from boss.pp.pp_main import PPMain 
from boss.pp.pf_main import PFMain  # <------ Needed for pareto calculations

# Define the user functions within some bounds
import numpy as np
bounds = np.array([[ 0.0 , 3.0 ], [ -2 , 0 ] ])

def f0(X):
    x1 = X[:,0][0]
    x2 = X[:,1][0]
    func = x1**4 + x2**4 + x1*x2 - (x1*x2)**2
    return -1*func

def f1(X):
    x1 = X[:,0][0]
    x2 = X[:,1][0]
    func = x1**4 + x2**4 + x1*x2 - (x1*x2)**2 - 10*x1**2
    return -1*func

#constants
#Number of points to make a GP model
iterpts = 50
#Mesh size to calculate the pareto optimal solution (pos) and paretofront (pf).
mesh_size = 30

# single run boss
bo0 = BOMain(
    f0,  # list all tasks
    bounds=bounds,
    num_tasks=1,
    kernel="rbf",
    iterpts=iterpts,
)
bo0.run()

# Another single run boss
bo1 = BOMain(
    f1,  # list all tasks
    bounds=bounds,
    num_tasks=1,
    kernel="rbf",
    iterpts=iterpts,
)
bo1.run()

#Collect the models for the pareto front (pf)
pf0 = PFMain(mesh_size = mesh_size, models = bo0.model)
pf1 = PFMain(mesh_size = mesh_size, models = bo1.model)

#combine them for Multi-objective optimisation
bo_MO = BOMain(
    [pf0.boss_functions(), pf1.boss_functions() ],  # list all tasks
    bounds=bounds,
    num_tasks=2,
    kernel="rbf",
    iterpts=0,
    task_initpts = [iterpts,iterpts],
)
bo_MO.run()


# Calculate the pareto front with mesh size 30 using GPs to fit the functions
pf = PFMain(n_tasks=2, bounds=bounds, mesh_size=30, models=bo_MO )
pf.run()

#Plot the pareto optimal solutions (POS)
pf.plot_pos(r"x$_1$", r"x$_2$", model1 = r"f$_0$", model2 = r"f$_1$")

#Plot the pareto front (PF)
pf.plot_pf(r"f$_0$", r"f$_1$")
