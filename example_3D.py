###################################################################################
# 2D example of a Pareto front
# An example to reproduce the pareto front as produced by mathlab in the link below
# https://www.mathworks.com/help/gads/pareto-front-for-two-objectives.html
# Here we calculate the pareto front using GP functions with an RBF kernel. 
###################################################################################

# Import
from boss.bo.bo_main import BOMain
#from boss.pp.pp_main import PPMain 
from boss.pp.pf_main import PFMain  # <------ Needed for pareto calculations

# Define the user functions within some bounds
import numpy as np
bounds = np.array([[ -5 , 5 ], [ -5 , 5 ], [-5,5] ])

def fun(x):
    x = np.reshape(x, -1)
    res = np.atleast_2d( np.array([
        np.dot(x - np.array([1, 2, 3]), x - np.array([1, 2, 3])),
        np.dot(x - np.array([-1, 3, -2]), x - np.array([-1, 3, -2])),
        np.dot(x - np.array([0, -1, 1]), x - np.array([0, -1, 1]))
    ]) )
    #print ( res )
    return res

def f0(X):
    return -1*fun(X)[:,0][0]

def f1(X):
    return -1*fun(X)[:,1][0]

def f2(X):
    return -1*fun(X)[:,2][0]

#constants
#Number of points to make a GP model
iterpts = 20
#Mesh size to calculate the pareto optimal solution (pos) and paretofront (pf).
mesh_size = 10
# Number of objectives or task
tasks = 3

# Run boss
bo0 = BOMain(
    f0,  # list all tasks
    bounds=bounds,
    num_tasks=1,
    kernel="rbf",
    iterpts=10,
)
bo0.run()

# Run boss
bo1 = BOMain(
    f1,  # list all tasks
    bounds=bounds,
    num_tasks=1,
    kernel="rbf",
    iterpts=10,
)
bo1.run()

# Run boss
bo2 = BOMain(
    f2,  # list all tasks
    bounds=bounds,
    num_tasks=1,
    kernel="rbf",
    iterpts=10,
)
bo2.run()

#Collect the models for the pareto front (pf)
pf0 = PFMain(mesh_size = mesh_size, models = bo0.model)
pf1 = PFMain(mesh_size = mesh_size, models = bo1.model)
pf2 = PFMain(mesh_size = mesh_size, models = bo2.model)

#combine them for Multi-objective optimisation
bo_MO = BOMain(
    [pf0.boss_functions(), pf1.boss_functions(), pf2.boss_functions() ],  # list all tasks
    bounds=bounds,
    num_tasks=tasks,
    kernel="rbf",
    iterpts=0,
    task_initpts = [iterpts,iterpts,iterpts],
)
bo_MO.run()

# Calculate the pareto front with mesh size 10 using GPs to fit the functions
pf = PFMain(n_tasks=tasks, bounds=bounds, mesh_size=mesh_size, models=bo_MO)
pf.run()

#Plot the pareto optimal solutions (POS)
pf.plot_pos(r"x$_1$", r"x$_2$", r"x$_3$", model1 = r"f$_0$", model2 = r"f$_1$", model3 = r"f$_2$")

#Plot the pareto front (PF)
pf.plot_pf(r"f$_0$", r"f$_1$", r"f$_2$", elev=9, azim=16)
