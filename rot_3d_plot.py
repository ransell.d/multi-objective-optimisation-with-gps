import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

data = np.loadtxt("y_feasible_output.dat")
pf = np.loadtxt("y_pareto_output.dat")
x = data[:, 0]
y = data[:, 1]
z = data[:, 2]
#Y_model = data[:,3]

# Create a 3D scatter plot
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111, projection="3d")
# Customize the scatter plot with colors, sizes, and transparency
scatter = ax.scatter(
    x, y, z, s=40, c=z, cmap="rainbow", alpha=0.5, label="3D Model"
)
cbar = plt.colorbar(scatter)

scatter = ax.scatter(
    pf[:, 0], pf[:, 1], pf[:, 2], s=80, label="Pareto Front",c = 'k', zorder = 10,
)

def animate_func(num):
    ax.view_init(elev=30, azim=num)
    #return fig


rotations = animation.FuncAnimation(fig, animate_func, frames=np.arange(0, 360, 1), interval=50)


#plt.show()
writer = animation.PillowWriter(fps=5, metadata=dict(artist="Ransell_DSouza"), bitrate=100)
rotations.save("example_3D.gif", writer=writer)
